*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${web_browser}         chrome
${url_login}           http://127.0.0.1:8080/
${link_studentEnp}     xpath=//a[@href="/manageStudentEnp"]
${url_studentAdd}      http://127.0.0.1:8080/manageStudentEnp_add
${email}               email
${email_value1}        pichet
${password}            password
${password_value1}     1234
${creditDown}          creditDownEnp
${creditDown_value1}   62
${btnOK}               btnok
${btnAdd}              btnAdd

*** Keywords ***
เปิดหน้าจอ
    [Arguments]         ${url_page}
    [Documentation]     ใช้เปิดหน้าจอ
    Open Browser        ${url_page}     ${web_browser}
กดลิงค์
    [Arguments]         ${url_link}
    [Documentation]     ใช้กดลิงค์ 
    Click Link          ${url_link}
กรอกข้อมูล
    [Arguments]         ${text}     ${value}
    [Documentation]     ใช้กรอกข้อมูล
    Input Text          ${text}     ${value}
กดปุ่ม
    [Arguments]         ${btn}
    [Documentation]     ใช้กดปุ่ม
    Click Button        ${btn}

*** Test Case ***  
TC-PSF-01 เปิดหน้าจอแล้ว Login เข้าสู่ระบบ
    เปิดหน้าจอ  ${url_login}
    กรอกข้อมูล  ${email}      ${email_value1} 
    กรอกข้อมูล  ${password}   ${password_value1}
    กดปุ่ม      ${btnOK}

TC-PSF-04-04-05 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลง
    กดลิงค์     ${link_studentEnp}
    กดปุ่ม      ${btnAdd}  
    เปิดหน้าจอ  ${url_studentAdd}
    กรอกข้อมูล  ${creditDown}      ${creditDown_value1}


