*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${web_browser}         chrome
${url_login}           http://127.0.0.1:8080/
${link_studentEnp}     xpath=//a[@href="/manageStudentEnp"]
${url_studentAdd}      http://127.0.0.1:8080/manageStudentEnp_add
${email}               email
${email_value1}        pichet
${password}            password
${password_value1}     1234
${id}                  studentIDEnp
${id_value1}           63160191
${name}                nameStuEnp
${name_value1}         ซาเนีย หวังใจ
${grade}               stuGpaEnp
${grade_value1}        4.00
${creditPass}          creditPassedEnp
${creditPass_value1}   5
${btnOK}               btnok
${btnAdd}              btnAdd
${label_level}         stuLevelEnp
${level1}              ตรี พิเศษ
${label_status}        stuStatusEnp
${status2}             12

*** Keywords ***
เปิดหน้าจอ
    [Arguments]         ${url_page}
    [Documentation]     ใช้เปิดหน้าจอ
    Open Browser        ${url_page}     ${web_browser}
กดลิงค์
    [Arguments]         ${url_link}
    [Documentation]     ใช้กดลิงค์ 
    Click Link          ${url_link}
กรอกข้อมูล
    [Arguments]         ${text}     ${value}
    [Documentation]     ใช้กรอกข้อมูล
    Input Text          ${text}     ${value}
กดปุ่ม
    [Arguments]         ${btn}
    [Documentation]     ใช้กดปุ่ม
    Click Button        ${btn}
เลือกข้อมูล
    [Arguments]         ${label}    ${Select_option}
    [Documentation]     ใช้เลือกข้อมูล
    Select From List By Label  ${label}     ${Select_option}

*** Test Case ***  
TC-PSF-01 เปิดหน้าจอแล้ว Login เข้าสู่ระบบ
    เปิดหน้าจอ  ${url_login}
    กรอกข้อมูล  ${email}      ${email_value1} 
    กรอกข้อมูล  ${password}   ${password_value1}
    กดปุ่ม      ${btnOK}

TC-PSF-04-04-15 ตรวจสอบการกรอกหากไม่กรอกข้อมูลหน่วยกิตที่ลง
    กดลิงค์     ${link_studentEnp}
    กดปุ่ม      ${btnAdd}  
    เปิดหน้าจอ  ${url_studentAdd}
    กรอกข้อมูล  ${id}    ${id_value1}
    เลือกข้อมูล  ${label_level}     ${level1}
    กรอกข้อมูล  ${grade}      ${grade_value1}
    กรอกข้อมูล  ${name}   ${name_value1}
    เลือกข้อมูล  ${label_status}     ${status2}
    กรอกข้อมูล  ${creditPass}     ${creditPass_value1}


